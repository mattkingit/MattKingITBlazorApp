﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MattKingITBlazorApp.Data
{
    public class MyBlazorDbContext : DbContext
    {
        protected IConfiguration _configuration;

        public MyBlazorDbContext()
        {
            _configuration = new ConfigurationBuilder().AddJsonFile(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\appsettings.json").Build();
        }

        public MyBlazorDbContext([NotNull] IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Loads all types from an assembly which have an interface of IBase and is a public class
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(s => s.GetInterfaces().Any(_interface => _interface.Equals(typeof(IBase)) && s.IsClass && !s.IsAbstract && s.IsPublic));

            foreach (var type in types)
            {
                // On Model Creating
                var onModelCreatingMethod = type.GetMethods().FirstOrDefault(x => x.Name == "OnModelCreating" && x.IsStatic);

                if (onModelCreatingMethod != null)
                {
                    onModelCreatingMethod.Invoke(type, new object[] { builder });
                }

                // On Base Model Creating
                if (type.BaseType == null || type.BaseType != typeof(Base))
                {
                    continue;
                }

                var baseOnModelCreatingMethod = type.BaseType.GetMethods().FirstOrDefault(x => x.Name == "OnModelCreating" && x.IsStatic);

                if (baseOnModelCreatingMethod == null)
                {
                    continue;
                }

                var baseOnModelCreatingGenericMethod = baseOnModelCreatingMethod.MakeGenericMethod(new Type[] { type });

                if (baseOnModelCreatingGenericMethod == null)
                {
                    continue;
                }

                baseOnModelCreatingGenericMethod.Invoke(typeof(Base), new object[] { builder });
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            // Sets the database connection from appsettings.json
            builder.UseSqlServer(_configuration["ConnectionStrings:MyBlazorDbContext"]);
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is IBase)
                {
                    if (entry.State == EntityState.Added)
                    {
                        entry.Property("Created").CurrentValue = DateTimeOffset.Now;
                    }
                    else if (entry.State == EntityState.Modified)
                    {
                        entry.Property("LastUpdated").CurrentValue = DateTimeOffset.Now;
                    }
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
