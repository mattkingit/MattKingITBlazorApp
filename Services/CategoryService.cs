﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using MattKingITBlazorApp.Data;
using MattKingITBlazorApp.Data.CategorySection;

namespace MattKingITBlazorApp.Services.CategorySection
{
    public partial class CategoryService : ICategoryService
    {
        protected readonly MyBlazorDbContext _context;

        public CategoryService([NotNull] MyBlazorDbContext context)
        {
            _context = context;
        }

        public virtual async Task<Category> GetAsync(int id)
        {
            return await _context.Set<Category>().EnabledCategories().FirstOrDefaultAsync(post => post.Id == id);
        }

        public virtual async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await _context.Set<Category>().EnabledCategories().ToListAsync();
        }
    }

    public static class CategoryServiceHelpers
    {
        public static IQueryable<Category> EnabledCategories(this IQueryable<Category> query)
        {
            return query.Where(category => category.Enabled && !category.Deleted.HasValue);
        }
    }
}
